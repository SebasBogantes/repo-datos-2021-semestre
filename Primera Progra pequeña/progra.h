#ifndef progra
#define progra

/**Sebastián Bogantes Rodríguez 2020028437
 * Instituto Tecnológico de Costa Rica
 * Estructura de Datos
 * Progra 1 pequeña
 */

struct lista_enlazada{
	struct nodo*	inicio;
};

struct nodo{
	int				vida;
	int 			ataque;
	char* 			equipo;
	struct nodo*	siguiente;
};
	

struct lista_enlazada* crear_lista();

int imprimir_lista(struct lista_enlazada*);

struct lista_enlazada* insertar_inicio(struct lista_enlazada* lista, char* equipo);

struct lista_enlazada* insertar_final(struct lista_enlazada* lista, char* equipo);

struct lista_enlazada* eliminar(struct lista_enlazada* lista);

struct lista_enlazada* atacar(struct lista_enlazada* lista, int posicion);

int recorrer_lista(struct lista_enlazada* lista);

int random_vida(int a, int b);

int random_ataque(int a, int b);

#endif
