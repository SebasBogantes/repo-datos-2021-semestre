#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <time.h>
#include "progra.h"

/**Sebastián Bogantes Rodríguez 2020028437
 * Instituto Tecnológico de Costa Rica
 * Estructura de Datos
 * Progra 1 pequeña
 * Nuwi-kun peguémonos los besos ya estoy desesperado
 */
 
char* EQUIPOROJO = "rojo";
char* EQUIPOAZUL = "azul";

// Crea la lista
struct lista_enlazada* crear_lista(){
	struct lista_enlazada* nueva_lista = calloc(1, sizeof(struct lista_enlazada));
	return nueva_lista;
}

// Crea el nodo (barco)
struct nodo* crear_nodo(char* equipo){
	struct nodo* nuevo_nodo = calloc(1, sizeof(struct nodo));
	nuevo_nodo->equipo = calloc(strlen(equipo), sizeof(char*));
	return nuevo_nodo;
}

// Imprime lista
int imprimir_lista(struct lista_enlazada* lista){
    
    // Variable
	struct nodo* actual = NULL;

	// Valida si la lista no existe
	if (lista == NULL){
		printf("Lista NULL\n");
		return -1;	
	}

	// Valida si no tiene un primer elemento	
	if (lista->inicio == NULL){
		printf("Lista vacía\n");
		return -1;
	}

	actual = lista->inicio;

	while (actual != NULL){
		printf("Vida:");
		printf("%d\tAtaque:", actual->vida);
		printf("%d Equipo: ", actual->ataque);
		printf("%s\t", actual->equipo);
		actual = actual->siguiente;
	}

	printf("\n");
	return 0;
}

//Inserta el nodo (barco) al inicio de la lista
struct lista_enlazada* insertar_inicio(struct lista_enlazada* lista, char* equipo){

	// Variable
	struct nodo* nuevo_nodo = NULL;

	// Valida si la lista no existe
	if (lista == NULL){
		return NULL;
	}

	// Crea el barco y le asigna sus atributos
	nuevo_nodo = crear_nodo(equipo);
	nuevo_nodo->vida = random_vida(8,15)+8;
	nuevo_nodo->ataque = random_ataque(1,7)+1;
	strcpy(nuevo_nodo->equipo, equipo);

	// Se inserta el nodo (barco) si la lista está vacía
	if (lista->inicio == NULL){
		lista->inicio = nuevo_nodo;
	}
	
	// Si no está vacía se asigna el nuevo nodo como primer elemento
	else{
		nuevo_nodo->siguiente = lista->inicio;
		lista->inicio = nuevo_nodo;
	}
	return lista;
}

// Inserta el nodo (barco) al final de la lista
struct lista_enlazada* insertar_final(struct lista_enlazada* lista, char* equipo){

    // Variables
	struct nodo* nuevo_nodo = NULL;
	struct nodo* actual = NULL;
	
    // Validar si lista está nula
	if (lista == NULL){
		return NULL;
	}
    
    // Crea nodo y le asigna el elemento
	nuevo_nodo = crear_nodo(equipo);
	nuevo_nodo->vida = random_vida(8,15)+8;
	nuevo_nodo->ataque = random_ataque(1,7)+1;
	strcpy(nuevo_nodo->equipo, equipo);

	// Si la lista está vacia agregamos el nuevo nodo
	if (lista->inicio == NULL){
		lista->inicio = nuevo_nodo;
		return lista;
	}

	// Si la lista no está vacia se recorre cada nodo hasta que uno apunte a nulo y se agrega
	actual = lista->inicio;

	while (actual->siguiente != NULL){
		actual = actual->siguiente;
	}

	actual->siguiente = nuevo_nodo;

	return lista;
}

// Elimina el barquito que no tenga vida
struct lista_enlazada* eliminar(struct lista_enlazada* lista){

    // Variables 
	struct nodo* actual = NULL;
	struct nodo* anterior = NULL;
	
    // Valida si la lista no existe
	if (lista == NULL){
		return NULL;
	}
    
	// Verifica si la lista está vacía
	if (lista->inicio == NULL){
		return NULL;
	}

	// Define el primer nodo
	actual = lista->inicio;
	
	// Si el primer nodo no tiene vida
	if (actual->vida <= 0){
		lista->inicio = actual->siguiente;
		free(actual);
		actual = NULL; 
		return lista;
	}

	// Verifica los demás nodos 
	anterior = actual;
	actual = actual->siguiente;

	while(actual != NULL){

        // Se borra el barco (nodo) si no tiene vida
		if (actual->vida <= 0){
			anterior->siguiente = actual->siguiente;
			free(actual);
			actual=NULL;
			return lista;
		}

		// Pasa al siguiente nodo
		anterior = actual;
		actual = actual->siguiente;
	}
	return NULL;
}

// Encuentra el barco y ataca con él
struct lista_enlazada* atacar(struct lista_enlazada* lista, int posicion){
	
	struct nodo* actual = NULL;
	struct nodo* anterior = NULL;
	int contador = 1;
	
	// Verifica si no existe la lista
	if (lista == NULL){
		return NULL;
	}
    
	// Verifica si la lista está vacía
	if (lista->inicio == NULL){
		return NULL;
	}

	// Define el primer nodo
	actual = lista->inicio;
	anterior = actual;
	actual = actual->siguiente;
		
	while(actual != NULL){
		// Si dispara con el primer nodo (barco)
		if ((contador==1) && (posicion == 1)){
			anterior->siguiente->vida = (anterior->siguiente->vida)-(anterior->ataque);
			return lista;
		}

		// Si dispara con el último nodo (barco)
		else if(actual->siguiente == NULL){
			if (contador == (posicion-1)){
				anterior->vida = (anterior->vida)-(actual->ataque);
				return lista;
				}	
			}
		// Si dispara con los demás nodos (barcos)	
		else{
			if (contador == posicion-1){
				anterior->vida = (anterior->vida)-(actual->ataque);
				actual->siguiente->vida = (actual->siguiente->vida)-(actual->ataque);
				return lista;
			}	
		}		
		contador += 1;
		anterior = actual;
		actual = actual->siguiente;
	}
	return NULL;
}

// Saca al ganador, no está bien hecha
int ganador(struct lista_enlazada* lista){
	
	// Variable
	int contadorazul = 3;
	int contadorrojo = 3;
	struct nodo* actual = NULL;

	// Valida si lista está nula
	if (lista == NULL){
		printf("Lista NULL\n");
		return -1;	
	}
	// Valida si no tiene un primer elemento	
	if (lista->inicio == NULL){
		printf("Lista vacía\n");
		return -1;
	}

	actual = lista->inicio;

	while (actual != NULL){
		if (strcmp(actual->equipo, EQUIPOROJO) == 0){
			contadorrojo -= 1;
			if (contadorrojo == 0){
				printf("El ganador es el equipo rojo");
				return 0;
			}
		}		
		else if(strcmp(actual->equipo, EQUIPOAZUL) == 0){
			contadorazul -= 1;
			if (contadorazul == 0){
				printf("El ganador es el equipo azul");
				return 0;
			}	
		}
		actual = actual->siguiente;
	}
}

// Recorre lista para luego valir en el main la longitud
int recorrer_lista(struct lista_enlazada* lista){
	
	struct nodo* actual = NULL;
	int indice = 0;
	
	// Valida si lista está nula
	if (lista == NULL){
		printf("Lista NULL\n");
		return -1;	
	}
	// Valida si no tiene un primer elemento	
	if (lista->inicio == NULL){
		printf("Lista vacía\n");
		return -1;
	}
	actual = lista->inicio;
	
	while (actual!=NULL){
		indice+=1;
		actual = actual->siguiente;
	}
	return indice;
}


// Cantidad de vida random 
int random_vida(int a, int b){
	return rand()%(b-a)+a;
}

// Cantidad de ataque random
int random_ataque(int a, int b){
	return rand()%(b-a)+a;
}

// Main
int main(){
	
	srand(time(NULL));
	struct lista_enlazada* lista = crear_lista();
	
	printf("------------------------------------------------\n");
	printf("Bienvenido a barquilandia\n");
	printf("Estos son sus barquitos\n");
	printf("------------------------------------------------\n");
	
	int verdadero = 1;
	int posicion  = 0;
	int longitud  = 0;
	int ganador   = 0;
	
	insertar_inicio(lista,"azul");	 
	insertar_final(lista, "rojo");
	insertar_final(lista, "azul");
	insertar_final(lista, "rojo");
	insertar_inicio(lista,"rojo");
	insertar_final(lista, "azul");
	imprimir_lista(lista);
	printf("------------------------------------------------\n");
	
	while (verdadero == 1){
		longitud = recorrer_lista(lista);
		printf("Digite el número del barquito con el que quiere atacar: ");
		scanf("%d\n", &posicion);
		if((posicion > 0) && (posicion <= longitud)){
			atacar(lista, posicion);
			eliminar(lista);
			longitud = 0;
		}	
		else{
			printf("No existe ese barquito\n");
		}
		posicion = 0;
		eliminar(lista);
		imprimir_lista(lista);
	}
	return 0;
}
