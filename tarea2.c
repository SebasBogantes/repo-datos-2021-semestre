/** Instituto Tecnológico de Costa Rica
 * Estructuras de Datos
 * Primer Semestre 2021
 * Tarea Corta 2
 * Sebastián Bogantes Rodríguez 2020028437
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**Elimina el caracter de un texto, recibe como parámetro el texto a 
* recorrer y un caracter deseado
* Nuwi peguémonos unos besos
*/

char* eliminarcaracter(char* texto, char caracter){
		
	int contador = 0;
	int contadortexto = 0;
	
	int largo = 0;
	largo = strlen(texto);
	
	// Crea espacio en memoria para el nuevo texto
	char* nuevotexto = calloc(largo + 1, sizeof(char));
	
	
	while (texto[contador] != '\0'){
		
		if (texto[contador] != caracter){
			nuevotexto[contadortexto]=texto[contador];
			contadortexto++;
		}
		contador++;
	}	
	printf("Su nueva palabra es: %s\n", nuevotexto);	
	return nuevotexto;
}

// Invierte un texto 
void invertir(char* texto){
	
	int i = 0;
	i = strlen(texto);
	
	while(i>=0){
		printf("%c", texto[i]);
		i--;
	}
	printf("\n");
}

// Copia el texto en el HEAP
void copiar(char *texto){
	int largo = strlen(texto);
	char* textoMover = calloc(largo + 1, sizeof(char));
	strcpy(textoMover, texto);
	printf("%s\n", textoMover); 
}

int main(){
	
	eliminarcaracter("caccacccacacac", 'c');
	invertir("viva la vida loca");
	copiar("VAMOS A FUMAR MARIHUANA");
	  
	return 0;
}
