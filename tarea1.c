/** Instituto Tecnológico de Costa Rica
 * Estructuras de Datos
 * Primer Semestre 2021
 * Tarea Corta 1 
 * Sebastián Bogantes Rodríguez 2020028437
 * Qué Nuwi, ¿Unos lolsitos?
 * Hay 7 de 9 ejercicios, faltan los de largo con ciclos
 */

// Ignore esta línea, gitlab es una empananda

#include <stdio.h>
#include <stdlib.h>

// Determina si un número es par
void numeropar(){
	
	int numero = 0; 
    	
    printf("Digite su número: ");
    scanf("%d", &numero);
    
    if (numero%2 == 0) {
	    printf("Su número es par");
	}
	
	else {
		printf("Su número no es par \n");
	}	
}	 

// Determina la suceción de fibonacci en un largo deseado
void fibonacci(){
	
	int a;
	int cantidad = 0;
	int primero = 0;
	int segundo = 1;
	int siguiente = 0;
	
	printf("Digite la cantidad deseada de terminos de la sucesión \n");
	scanf ("%d", &cantidad);
	
	for (a = 0; a < cantidad; a++){
		if (a <= 1){
		  siguiente = a;
		  }
		else{
		siguiente = primero + segundo;
		primero = segundo;
		segundo = siguiente;
		} 
    printf("%d\n", siguiente);
  }	
}

// Determina el factorial de un número
void factorial(){
	
	int factorial = 1;
	int numero = 0;
	int i = 1;
	
	printf("Digite un número para hallar su factorial: \n");
	scanf("%d", &numero);
	
	for(i;i<=numero;i++){
		factorial = factorial * i;
	}
	printf("El factorial es: %d\n", factorial);	
}

// Da la sumatoria iterativa de 0 a n con un ciclo for
void sumatoriafor(){
	
	int numero = 0;
	int i = 0;
	int sumatoria = 0;
	
	printf("Digite el numero a determinar su sumatoria iterativa: \n");
	scanf("%d\n", &numero);
	
	for(i;i<=numero;i++){
		sumatoria = sumatoria + i;
	}	
	printf("La sumatoria es: %d\n", sumatoria);
}

// Da la sumatoria iterativa de 0 a n con un ciclo while
void sumatoriawhile(){
	
	int numero = 0;
	int i = 0;
	int sumatoria = 0;
	
	printf("Digite el numero a determinar su sumatoria while: \n");
	scanf("%d\n", &numero);
	
	if (numero < 0){
		printf("No quería programarlo más mucha paja\n");
	}
		
	else{ 
		while(i<=numero){
			sumatoria = sumatoria + i;
			i++;
			printf("%d\n", sumatoria);
		}	
		printf("La sumatoria es: %d\n", sumatoria);
	}
}

// Invierte un número
int invertir(int numero){
	
	int respuesta = 0;
	
	while (numero > 0){
		respuesta = respuesta + numero % 10;
		respuesta = respuesta * 10;
		numero = numero / 10;
	}
	
	return respuesta / 10;
}	

// Averigua si es palíndromo o no
void numeropalindromo(){
	int numero = 0;
	
	printf("Digite un número para determinar su validez\n");
	scanf("%d\n", &numero);
	
	if (numero < 0){
		printf("No se me pase de listo papi\n");
	}
	
	else if (numero == invertir(numero)){
		printf("La pura mamba ese número\n");
	}
	
	else{
		printf("mamó papi no es\n");
	}
}
	
// Main xd
int main(){
    //numeropar();
    //fibonacci();
    //factorial();
    //sumatoriafor();
    //sumatoriawhile();
    numeropalindromo();
    return 0;		
}



	/**int MAX = 10;
    char buf[MAX]; 
    fgets(buf, MAX, stdin); 
    printf("string is: %s\n", buf); 
    * 
    * void eliminarc(char c){
	
	int i = 0;
	char* texto = "cacaacccacacaaaa";
	char caracter = texto[0];
	printf("El resultado es:\n");
	
	while (caracter != '\0'){
		caracter = texto[i];
		if (caracter != c){
			printf("%c", caracter);
		}
		i++;
	}
}
    */

